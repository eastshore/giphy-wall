package software.eastshore.gifwall.giphy

import android.util.Log
import androidx.paging.PositionalDataSource
import com.giphy.sdk.core.models.Media
import com.giphy.sdk.core.models.enums.MediaType
import com.giphy.sdk.core.models.enums.RatingType
import com.giphy.sdk.core.network.api.CompletionHandler
import com.giphy.sdk.core.network.api.GPHApiClient
import com.giphy.sdk.core.network.engine.ApiException
import com.giphy.sdk.core.network.response.ListMediaResponse
import java.util.concurrent.Future

class PositionalSearchSource(
    private val client: GPHApiClient,
    private val query: String?,
    private val rating: RatingType
) : PositionalDataSource<Media>() {
    private val futures: MutableSet<Future<*>> = mutableSetOf()

    override fun loadInitial(params: LoadInitialParams, callback: LoadInitialCallback<Media>) =
        loadInitialOrDie(params, callback)

    private var shouldAlive = true

    private fun loadInitialOrDie(
        params: LoadInitialParams,
        callback: LoadInitialCallback<Media>,
        nth: Int = 0
    ) {
        searchOrTrending(
            query = query,
            limit = params.requestedLoadSize,
            offset = params.requestedStartPosition,
            rating = rating,
        ) { res, err ->
            if (res != null) {
                res.pagination
                    .let { pagination ->
                        if (pagination != null) {
                            callback.onResult(
                                data = res.data ?: emptyList(),
                                position = pagination.offset ?: params.requestedStartPosition,
                                totalCount = pagination.totalCount
                            )
                        } else {
                            callback.onResult(
                                res.data ?: emptyList(),
                                params.requestedStartPosition
                            )
                        }
                    }
            }

            if (err != null) {
                if (nth < MAX_TRIES && shouldAlive) {
                    if (err is ApiException && err.errorResponse.meta.status == 414) {
                        Log.e(javaClass.simpleName, "This query is too long: [$query]")
                        callback.onResult(
                            data = emptyList(),
                            position = 0,
                            totalCount = 0
                        )
                    } else loadInitialOrDie(params, callback, nth + 1)
                } else {
                    Log.e(javaClass.simpleName, "Error while loading results", err)
                    callback.onResult(
                        data = emptyList(),
                        position = params.requestedStartPosition,
                        totalCount = 0
                    )
                }
            }
        }.let(futures::add)
    }

    override fun loadRange(params: LoadRangeParams, callback: LoadRangeCallback<Media>) =
        loadRangeOrDie(params, callback)

    private fun loadRangeOrDie(
        params: LoadRangeParams,
        callback: LoadRangeCallback<Media>,
        nth: Int = 0
    ) {
        searchOrTrending(
            query = query,
            limit = params.loadSize,
            offset = params.startPosition,
            rating = rating,
        ) { res, err ->
            if (res != null) {
                callback.onResult(data = res.data ?: emptyList())
            }

            if (err != null) {
                if (nth < MAX_TRIES && shouldAlive) {
                    loadRangeOrDie(params, callback, nth + 1)
                } else {
                    Log.e(javaClass.simpleName, "Error while loading results", err)
                    callback.onResult(emptyList())
                }
            }
        }.let(futures::add)
    }

    private fun searchOrTrending(
        query: String?,
        limit: Int,
        offset: Int,
        rating: RatingType,
        callback: CompletionHandlerLambda<ListMediaResponse>
    ): Future<*> =
        if (query.isNullOrBlank()) {
            Log.v(
                javaClass.simpleName,
                "Get page of Trending results starting [$offset], limit [$limit]"
            )
            client.trending(
                MediaType.gif,
                limit,
                offset,
                rating,
                object : CompletionHandler<ListMediaResponse> {
                    override fun onComplete(result: ListMediaResponse?, e: Throwable?) {
                        callback(result, e)
                    }
                })
        } else {
            Log.v(
                javaClass.simpleName,
                "Get page of search results for [$query] starting [$offset], limit [$limit]"
            )
            client.searchGif(
                query = this.query ?: "",
                limit = limit,
                offset = offset,
                rating = rating,
                callback
            )
        }

    fun cancelAll() {
        shouldAlive = false
        futures.removeAll { it.cancel(true) }
    }

    companion object {
        private const val MAX_TRIES = 3;
    }
}