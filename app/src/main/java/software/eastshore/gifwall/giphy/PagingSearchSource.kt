package software.eastshore.gifwall.giphy

import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.giphy.sdk.core.models.Media
import com.giphy.sdk.core.models.enums.MediaType
import com.giphy.sdk.core.models.enums.RatingType
import com.giphy.sdk.core.network.api.CompletionHandler
import com.giphy.sdk.core.network.api.GPHApiClient
import com.giphy.sdk.core.network.response.ListMediaResponse
import kotlinx.coroutines.delay
import java.util.concurrent.Future

class PagingSearchSource(
    private val client: GPHApiClient,
    private val query: String?,
    private val rating: RatingType
) : PagingSource<Int, Media>() {
    private var pageSize: Int = -1

    override fun getRefreshKey(state: PagingState<Int, Media>): Int? {
        val effectivePageSize = if (pageSize < 0) {
            Log.w(javaClass.simpleName, "Using fake page size of 25 - shouldn't happen")
            25
        } else {
            pageSize
        }

        val key = state.anchorPosition?.let { pos -> (pos / effectivePageSize) * effectivePageSize }

        Log.v(javaClass.simpleName, "Refresh key for ${state.anchorPosition} is $key")

        return key
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Media> {
        Log.v(javaClass.simpleName, String.format("Fetching page %d of results for query [%s]", params.key ?: 0, query))
        val offset = params.key ?: 0

        var pageResult: LoadResult<Int, Media>? = null

        val fut = searchOrTrending(
            query = query,
            limit = params.loadSize,
            offset = offset,
            rating = rating
        ) { result, err ->
            pageResult = if (result != null) {
                Log.v(javaClass.simpleName, "Got results offset [$offset] of total [${result.pagination?.totalCount}]")

                val resultsCount = result.data?.size ?: result.pagination?.count ?: 0

                if (pageSize < 0) {
                    pageSize = resultsCount
                }

                val itemsAfter = result.pagination?.totalCount?.let { tc ->
                    tc - (result.pagination?.offset ?: offset) - resultsCount
                }

                val nextItem = if (itemsAfter != null && itemsAfter > 0) {
                    offset + (result.pagination?.count ?: resultsCount)
                } else {
                    null
                }

                if (itemsAfter != null) {
                    Log.v(
                        javaClass.simpleName, """"
                        {
                            data = [${(result.data ?: emptyList()).size} items],
                            prevKey = null,
                            nextKey = $nextItem,
                            itemsAfter = ${0.coerceAtLeast(itemsAfter)},
                            itemsBefore = ${0.coerceAtLeast(offset)}
                        }""".trimIndent()
                    )
                    LoadResult.Page(
                        data = result.data ?: emptyList(),
                        prevKey = null,
                        nextKey = nextItem,
                        itemsAfter = 0.coerceAtLeast(itemsAfter),
                        itemsBefore = 0.coerceAtLeast(offset)
                    )
                } else {
                    Log.v(
                        javaClass.simpleName, """
                        {
                            data = [${(result.data ?: emptyList()).size} items],
                            nextKey = $nextItem
                        }""".trimIndent()
                    )
                    LoadResult.Page(
                        data = result.data ?: emptyList(),
                        prevKey = null,
                        nextKey = nextItem
                    )
                }
            } else if (err != null) {
                Log.w(javaClass.simpleName, "Error loading data!!!", err)
                LoadResult.Error(err)
            } else {
                null
            }
        }

        while (!fut.isDone) {
            delay(10)
        }

        Log.v(javaClass.simpleName, "Finished waiting for results to return for offset $offset")

        return pageResult ?: emptyPageError
    }

    private fun searchOrTrending(
        query: String?,
        limit: Int,
        offset: Int,
        rating: RatingType,
        callback: CompletionHandlerLambda<ListMediaResponse>
    ): Future<*> =
        if (query.isNullOrBlank()) {
            Log.v(javaClass.simpleName, "Get page of Trending results starting [$offset], limit [$limit]")
            client.trending(
                MediaType.gif,
                limit,
                offset,
                rating,
                object : CompletionHandler<ListMediaResponse> {
                    override fun onComplete(result: ListMediaResponse?, e: Throwable?) {
                        callback(result, e)
                    }
                })
        } else {
            Log.v(javaClass.simpleName, "Get page of search results for [$query] starting [$offset], limit [$limit]")
            client.searchGif(query = this.query ?: "", limit = limit, offset = offset, rating = rating, callback)
        }

    private val emptyPageError: LoadResult.Error<Int, Media>
        get() = LoadResult.Error(java.lang.RuntimeException("Could not load gif result!"))
}