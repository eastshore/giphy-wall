package software.eastshore.gifwall.giphy

import androidx.recyclerview.widget.DiffUtil
import com.giphy.sdk.core.models.Media
import dagger.Reusable
import javax.inject.Inject

@Reusable
class MediaDiffCallback @Inject constructor() : DiffUtil.ItemCallback<Media>() {
    override fun areItemsTheSame(oldItem: Media, newItem: Media): Boolean =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: Media, newItem: Media): Boolean =
        oldItem.id == newItem.id
                && oldItem.url == newItem.url
                && oldItem.images.original?.gifUrl == newItem.images.original?.gifUrl
}