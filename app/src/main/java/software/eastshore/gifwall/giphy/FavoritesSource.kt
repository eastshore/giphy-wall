package software.eastshore.gifwall.giphy

import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.giphy.sdk.core.models.Media
import com.giphy.sdk.core.network.api.CompletionHandler
import com.giphy.sdk.core.network.api.GPHApiClient
import com.giphy.sdk.core.network.response.ListMediaResponse
import com.giphy.sdk.tracking.position
import kotlinx.coroutines.channels.ChannelResult
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.*
import java.util.*
import java.util.concurrent.Future

class FavoritesSource(
    private val client: GPHApiClient,
    private val favoriteIds: List<String>
) : PagingSource<Int, Media>() {
    override fun getRefreshKey(state: PagingState<Int, Media>): Int? =
        state.anchorPosition
            ?.let(state::closestPageToPosition)
            ?.nextKey
            ?.let { (it - 1).coerceAtLeast(0) }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Media> {
        val page = params.key ?: 0
        val offset = params.loadSize * page

        Log.v(
            TAG,
            "Loading page $page, yielding offset $offset, size ${params.loadSize} (total ${favoriteIds.size}"
        )

        return favoriteIds
            .drop(offset)
            .take(params.loadSize)
            .takeIf(List<String>::isNotEmpty)
            ?.let(::fetch)
            ?.map { result ->
                Log.v(TAG, "Got data for page $page, offset $offset - ${result.data?.size} items")
                result.data
                    ?.forEachIndexed { i, m ->
                        m.position = i
                    }

                val mediaItems: List<Media> = (result.data ?: emptyList())
                val hasMore = mediaItems.size == params.loadSize
                        && favoriteIds.size > mediaItems.size

                Log.v(TAG, "prev: ${if (page == 0) null else page - 1}")
                Log.v(TAG, "next: ${if (hasMore) page + 1 else null}")
                Log.v(
                    TAG,
                    "after: ${(favoriteIds.size - offset - params.loadSize).coerceAtLeast(0)}"
                )


                LoadResult.Page(
                    data = mediaItems,
                    prevKey = if (page == 0) null else page - 1,
                    nextKey = if (hasMore) page + 1 else null,
                    itemsAfter = (favoriteIds.size - offset - params.loadSize).coerceAtLeast(0),
                    itemsBefore = offset
                )
            }
            ?.first()
            ?: LoadResult.Page(
                data = emptyList(),
                prevKey = if (page == 0) null else page - 1,
                nextKey = null,
                itemsAfter = 0,
                itemsBefore = offset
            )
    }

    private fun fetch(favoriteIds: List<String>): Flow<ListMediaResponse> =
        callbackFlow {
            val future: Future<*> = client.gifsByIds(
                favoriteIds,
                object : CompletionHandler<ListMediaResponse> {
                    override fun onComplete(result: ListMediaResponse?, e: Throwable?) {
                        if (e != null) {
                            close(e)
                        } else if (result != null) {
                            trySend(result)
                                .takeIf(ChannelResult<Unit>::isFailure)
                                ?.apply {
                                    exceptionOrNull()
                                        ?.also { close(it) }
                                        ?: close(
                                            RuntimeException(
                                                "Unknown error in fetching results?"
                                            )
                                        )
                                }
                            close()
                        } else {
                            close(RuntimeException("No result?"))
                        }
                    }
                }
            )
            awaitClose {
                future.cancel(true)
            }
        }

    companion object {
        private val TAG = FavoritesSource::class.simpleName
    }
}