package software.eastshore.gifwall.giphy

import android.view.View
import android.widget.ImageView
import com.bumptech.glide.RequestManager
import com.giphy.sdk.core.models.enums.MediaType
import com.giphy.sdk.core.models.enums.RatingType
import com.giphy.sdk.core.network.api.CompletionHandler
import com.giphy.sdk.core.network.api.GPHApiClient
import com.giphy.sdk.core.network.response.ListMediaResponse
import software.eastshore.gifwall.R
import java.util.concurrent.Future

typealias CompletionHandlerLambda<T> = (T?, Throwable?) -> Unit

fun GPHApiClient.searchGif(
    query: String,
    limit: Int? = null,
    offset: Int? = null,
    rating: RatingType? = null,
    handler: CompletionHandlerLambda<ListMediaResponse>
): Future<*> =
    search(
        query,
        MediaType.gif,
        limit,
        offset,
        rating,
        null,
        object : CompletionHandler<ListMediaResponse> {
            override fun onComplete(result: ListMediaResponse?, e: Throwable?): Unit = handler(result, e)
        })

interface HasGlide {
    val glide: RequestManager
}

fun <T : ImageView> HasGlide.loadGiphyAttribution(findViewById: (Int) -> T, viewResource: Int) {
    glide.load(R.raw.powered_by_giphy_small).into(findViewById(viewResource))
}