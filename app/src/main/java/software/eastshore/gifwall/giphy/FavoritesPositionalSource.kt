package software.eastshore.gifwall.giphy

import android.util.Log
import androidx.paging.PositionalDataSource
import com.giphy.sdk.core.models.Media
import com.giphy.sdk.core.network.api.CompletionHandler
import com.giphy.sdk.core.network.api.GPHApiClient
import com.giphy.sdk.core.network.response.ListMediaResponse
import com.giphy.sdk.tracking.position
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import java.util.*
import java.util.concurrent.Future
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

class FavoritesPositionalSource(
    private val client: GPHApiClient,
    private val favoriteIds: List<String>,
    private val coroutineScope: CoroutineScope,
    private val maxRetries: Int = 3,
    private val delayMillis: Long = 500L
) : PositionalDataSource<Media>() {

    override fun loadInitial(params: LoadInitialParams, callback: LoadInitialCallback<Media>) {
        coroutineScope.launch {
            favoriteIds.drop(params.requestedStartPosition)
                .takeIf(List<String>::isNotEmpty)
                ?.take(params.pageSize)
                .let {
                    repeat(maxRetries) { n ->
                        try {
                            return@let fetchSuspend(it)
                        } catch (t: Throwable) {
                            if (n == (maxRetries - 1)) {
                                throw t
                            }
                            delay(delayMillis * Math.pow(2.0, n.toDouble()).toInt())
                        }
                    }
                    emptyList()
                }
                .also(applyPosition(params.requestedStartPosition))
                .let {
                    Log.v(
                        TAG,
                        "Initial Result for ${params.requestedStartPosition}, ${params.pageSize}: ${it.size} of total ${favoriteIds.size}"
                    )
                    callback.onResult(
                        data = it,
                        position = params.requestedStartPosition,
                        totalCount = favoriteIds.size
                    )
                }
        }
    }

    override fun loadRange(params: LoadRangeParams, callback: LoadRangeCallback<Media>) {
        coroutineScope.launch {
            favoriteIds.drop(params.startPosition)
                .takeIf(List<String>::isNotEmpty)
                ?.take(params.loadSize)
                .let {
                    repeat(maxRetries) { n ->
                        try {
                            return@let fetchSuspend(it)
                        } catch (t: Throwable) {
                            if (n == maxRetries - 1) {
                                throw t
                            }
                            delay(delayMillis * Math.pow(2.0, n.toDouble()).toInt())
                        }
                    }
                    emptyList()
                }
                .also(applyPosition(params.startPosition))
                .let {
                    Log.v(
                        TAG,
                        "Range Result for ${params.startPosition}, ${params.loadSize}: ${it.size} of total ${favoriteIds.size}"
                    )
                    callback.onResult(it)
                }
        }
    }

    private suspend fun fetchSuspend(favoriteIds: List<String>?): List<Media> =
        suspendCancellableCoroutine { cont ->
            if (favoriteIds == null) {
                cont.resume(emptyList())
            } else {
                val future = client.gifsByIds(
                    favoriteIds,
                    object : CompletionHandler<ListMediaResponse> {
                        override fun onComplete(result: ListMediaResponse?, e: Throwable?) {
                            if (e != null) {
                                cont.resumeWithException(e)
                            } else if (result != null) {
                                cont.resume(result.data ?: emptyList())
                            } else {
                                @Suppress("ThrowableNotThrown")
                                cont.resumeWithException(java.lang.RuntimeException("No result?"))
                            }
                        }

                    }
                )

                cont.invokeOnCancellation {
                    future.cancel(true)
                }
            }
        }

    fun dump() {
        dumpster.removeAll { it.cancel(true) }
    }

    private val dumpster: MutableSet<Future<*>> = Collections.newSetFromMap(WeakHashMap())

    companion object {
        private val TAG = FavoritesPositionalSource::class.simpleName

        private fun applyPosition(startPosition: Int): (List<Media>) -> Unit = { result ->
            result.forEachIndexed { i, media -> media.position = i + startPosition }
        }
    }
}