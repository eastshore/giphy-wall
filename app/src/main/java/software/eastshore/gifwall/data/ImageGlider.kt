package software.eastshore.gifwall.data

import android.graphics.drawable.Drawable
import com.bumptech.glide.request.Request
import com.bumptech.glide.request.target.SizeReadyCallback
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.request.transition.Transition
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.channels.trySendBlocking
import java.io.File

class ImageGlider(private val channel: SendChannel<File>) : Target<File> {
    override fun onStart() {
    }

    override fun onStop() {
        channel.close()
    }

    override fun onDestroy() {
        channel.close()
    }

    override fun onLoadStarted(placeholder: Drawable?) {
    }

    override fun onLoadFailed(errorDrawable: Drawable?) {
        channel.close()
    }

    override fun onResourceReady(resource: File, transition: Transition<in File>?) {
        channel.trySendBlocking(resource)
    }

    override fun onLoadCleared(placeholder: Drawable?) {
    }

    override fun getSize(cb: SizeReadyCallback) {
        // Stub - do this to just get rid of it
        cb.onSizeReady(0, 0)
    }

    override fun removeCallback(cb: SizeReadyCallback) {
    }

    private var request: Request? = null

    override fun setRequest(request: Request?) {
        this.request = request
    }

    override fun getRequest(): Request? = request
}