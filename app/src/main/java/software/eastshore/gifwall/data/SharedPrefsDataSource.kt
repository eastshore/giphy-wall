package software.eastshore.gifwall.data

import android.content.SharedPreferences
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.withContext
import software.eastshore.gifwall.di.AppModule

class SharedPrefsDataSource(
    val prefs: SharedPreferences,
    private val coroutineScope: CoroutineScope,
    @AppModule.IODispatcher val ioDispatcher: CoroutineDispatcher
) {
    val onPrefsChangeStream: Flow<Update> by lazy {
        callbackFlow {
            val listener =
                SharedPreferences.OnSharedPreferenceChangeListener { sp, str ->
                    if (sp != null && str != null) {
                        trySend(Update(sp, str))
                            .takeIf { it.isFailure }
                            ?.apply {
                                // On failure, pass failure along
                                close(
                                    exceptionOrNull() ?: RuntimeException("Could not send Update")
                                )
                            }
                    }
                }

            prefs.registerOnSharedPreferenceChangeListener(listener)
            awaitClose { prefs.unregisterOnSharedPreferenceChangeListener(listener) }
        }.shareIn(
            coroutineScope,
            SharingStarted.WhileSubscribed(
                stopTimeoutMillis = 0L,
                replayExpirationMillis = 0L
            )
        )
    }

    inline fun <reified T> watchField(
        field: String,
        crossinline getter: ((SharedPreferences, String) -> T)
    ): Flow<T> =
        onPrefsChangeStream
            .filter { it.key == field }
            .map {
                withContext(ioDispatcher) {
                    getter(it.prefs, it.key)
                }
            }
            .onStart {
                if (null is T || prefs.contains(field)) {
                    emit(getter(prefs, field))
                }
            }

    inline fun update(crossinline setter: ((SharedPreferences.Editor) -> Unit)): Unit =
        prefs.edit()
            .also(setter)
            .apply()

    suspend inline fun updateAsync(crossinline setter: ((SharedPreferences.Editor) -> Unit)): Unit =
        coroutineScope {
            withContext(ioDispatcher) {
                update(setter)
            }
        }

    inline fun <T> get(crossinline getter: (SharedPreferences) -> T) = getter(prefs)

    class Update(val prefs: SharedPreferences, val key: String)
}