package software.eastshore.gifwall

import android.content.Context
import android.os.StrictMode
import androidx.multidex.MultiDexApplication
import software.eastshore.gifwall.di.ApplicationComponent
import software.eastshore.gifwall.di.DaggerApplicationComponent
import javax.inject.Inject

class GIFWallApplication : MultiDexApplication() {
    @Inject
    lateinit var strictThreadPolicy: StrictMode.ThreadPolicy

    @Inject
    lateinit var strictVmPolicy: StrictMode.VmPolicy

    override fun onCreate() {
        super.onCreate()

        component.inject(this)

        if (BuildConfig.DEBUG) {
            StrictMode.setThreadPolicy(strictThreadPolicy)
            StrictMode.setVmPolicy(strictVmPolicy)
        }
    }

    val component: ApplicationComponent by lazy { DaggerApplicationComponent.builder().binds(this).build() }
}

val Context.applicationComponent: ApplicationComponent
    get() = (applicationContext as GIFWallApplication).component


