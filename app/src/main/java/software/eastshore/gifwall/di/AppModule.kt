package software.eastshore.gifwall.di

import android.app.Application
import android.content.ContentResolver
import android.content.Context
import android.content.SharedPreferences
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import dagger.Module
import dagger.Provides
import dagger.Reusable
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import software.eastshore.gifwall.BuildConfig
import software.eastshore.gifwall.GIFWallApplication
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
object AppModule {
    @Provides
    @AppContext
    fun provideAppContext(app: Application): Context = app

    @Provides
    fun provideApp(app: GIFWallApplication): Application = app

    @AppContext
    @Reusable
    @Provides
    fun providesGlideLoader(@AppContext appContext: Context): RequestManager =
        Glide.with(appContext)

    @Provides
    @Reusable
    @AppContext
    fun providesContentResolver(@AppContext context: Context): ContentResolver =
        context.contentResolver

    @Provides
    @Reusable
    @AppContext
    fun providesSharedPreferences(@AppContext context: Context): SharedPreferences =
        context.getSharedPreferences(BuildConfig.APPLICATION_ID + "prefs", Context.MODE_PRIVATE)

    @Provides
    @Singleton
    @IODispatcher
    fun providesIODispatcher(): CoroutineDispatcher = Dispatchers.IO

    @Provides
    @Singleton
    @MainDispatcher
    fun providesMainDispatcher(): CoroutineDispatcher = Dispatchers.Main

    @Qualifier
    annotation class AppContext

    @Qualifier
    annotation class IODispatcher

    @Qualifier
    annotation class MainDispatcher
}