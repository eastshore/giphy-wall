package software.eastshore.gifwall.di

import android.content.Context
import androidx.recyclerview.widget.DiffUtil
import com.giphy.sdk.core.GiphyCore
import com.giphy.sdk.core.models.Media
import com.giphy.sdk.core.network.api.GPHApiClient
import dagger.Module
import dagger.Provides
import software.eastshore.gifwall.BuildConfig
import software.eastshore.gifwall.giphy.MediaDiffCallback

@Module
object GiphyModule {
    @Provides
    fun provideGiphyClient(@AppModule.AppContext appContext: Context): GPHApiClient {
        GiphyCore.configure(appContext, BuildConfig.GIPHY_API_KEY.trim())
        return GiphyCore.apiClient
    }

    @Provides
    fun providesMediaDiffCallback(callback: MediaDiffCallback): DiffUtil.ItemCallback<Media> = callback
}