package software.eastshore.gifwall.di

import android.os.Build
import android.os.StrictMode
import androidx.annotation.RequiresApi
import dagger.Module
import dagger.Provides
import dagger.Reusable

@Module
object StrictModeModule {
    @Provides
    @Reusable
    fun providesThreadPolicy(): StrictMode.ThreadPolicy =
        StrictMode.ThreadPolicy.Builder()
            .detectAll()
            .penaltyLog()
            .build()

    @Provides
    @Reusable
    fun providesVmPolicy(): StrictMode.VmPolicy = when {
        Build.VERSION.SDK_INT >= Build.VERSION_CODES.S -> providesVmPolicyS()
        Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q -> providesVmPolicyQ()
        Build.VERSION.SDK_INT >= Build.VERSION_CODES.O -> providesVmPolicyO()
        Build.VERSION.SDK_INT >= Build.VERSION_CODES.M -> providesVmPolicyM()
        else -> providesVmPolicyOld()
    }
        .penaltyLog()
        .build()

    private fun providesVmPolicyOld(): StrictMode.VmPolicy.Builder =
        StrictMode.VmPolicy.Builder()
            .detectActivityLeaks()
            .detectFileUriExposure()
            .detectLeakedClosableObjects()
            .detectLeakedRegistrationObjects()
            .detectLeakedSqlLiteObjects()

    @RequiresApi(Build.VERSION_CODES.S)
    private fun providesVmPolicyS(): StrictMode.VmPolicy.Builder =
        StrictMode.VmPolicy.Builder()
            .detectActivityLeaks()
            .detectCleartextNetwork()
            .detectFileUriExposure()
            .detectContentUriWithoutPermission()
            .detectCredentialProtectedWhileLocked()
            .detectImplicitDirectBoot()
            .detectIncorrectContextUse()
            .detectLeakedClosableObjects()
            .detectLeakedRegistrationObjects()
            .detectLeakedSqlLiteObjects()
            //.detectNonSdkApiUsage() // triggered by gson
            .detectUnsafeIntentLaunch()
            //.detectUntaggedSockets() // triggerd by glide

    @RequiresApi(Build.VERSION_CODES.Q)
    private fun providesVmPolicyQ(): StrictMode.VmPolicy.Builder =
        StrictMode.VmPolicy.Builder()
            .detectActivityLeaks()
            .detectCleartextNetwork()
            .detectFileUriExposure()
            .detectContentUriWithoutPermission()
            .detectCredentialProtectedWhileLocked()
            .detectImplicitDirectBoot()
            .detectLeakedClosableObjects()
            .detectLeakedRegistrationObjects()
            .detectLeakedSqlLiteObjects()
            //.detectNonSdkApiUsage()  triggered by gson
            //.detectUntaggedSockets() // triggerd by glide

    @RequiresApi(Build.VERSION_CODES.O)
    private fun providesVmPolicyO(): StrictMode.VmPolicy.Builder =
        StrictMode.VmPolicy.Builder()
            .detectActivityLeaks()
            .detectCleartextNetwork()
            .detectFileUriExposure()
            .detectContentUriWithoutPermission()
            .detectLeakedClosableObjects()
            .detectLeakedRegistrationObjects()
            .detectLeakedSqlLiteObjects()
            //.detectUntaggedSockets() // triggerd by glide

    @RequiresApi(Build.VERSION_CODES.M)
    private fun providesVmPolicyM(): StrictMode.VmPolicy.Builder =
        StrictMode.VmPolicy.Builder()
            .detectActivityLeaks()
            .detectCleartextNetwork()
            .detectFileUriExposure()
            .detectLeakedClosableObjects()
            .detectLeakedRegistrationObjects()
            .detectLeakedSqlLiteObjects()
}