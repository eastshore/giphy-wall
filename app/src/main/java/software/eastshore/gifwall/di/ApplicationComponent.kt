package software.eastshore.gifwall.di

import dagger.BindsInstance
import dagger.Component
import software.eastshore.gifwall.GIFWallApplication
import software.eastshore.gifwall.ui.closeup.CloseupViewModel
import software.eastshore.gifwall.ui.main.*
import javax.inject.Singleton

@Singleton
@Component(modules = [GiphyModule::class, AppModule::class, GIFWallGlideModule::class, StrictModeModule::class])
interface ApplicationComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun binds(app: GIFWallApplication): Builder
        fun build(): ApplicationComponent
    }

    val activityComponentBuilder: ActivityComponent.Builder

    fun inject(viewModel: MainViewModel)
    fun inject(closeupViewModel: CloseupViewModel)
    fun inject(gifWallApplication: GIFWallApplication)
    fun inject(galleryFragment: GalleryFragment)
    fun inject(favoritesFragment: FavoritesFragment)
    fun inject(favoritesViewModel: FavoritesViewModel)
}