package software.eastshore.gifwall.di

import androidx.appcompat.app.AppCompatActivity
import dagger.BindsInstance
import dagger.Subcomponent
import software.eastshore.gifwall.ui.closeup.CloseupActivity
import software.eastshore.gifwall.ui.main.MainActivity
import software.eastshore.gifwall.ui.main.MainViewModel

@Subcomponent(modules = [ActivityModule::class])
interface ActivityComponent {
    @Subcomponent.Builder
    interface Builder {
        @BindsInstance
        fun binds(activity: AppCompatActivity): Builder
        fun build(): ActivityComponent
    }

    fun inject(mainActivity: MainActivity)
    fun inject(viewModel: MainViewModel)
    fun inject(closeupActivity: CloseupActivity)
}
