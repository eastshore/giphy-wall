package software.eastshore.gifwall.di

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import dagger.Module
import dagger.Provides
import dagger.Reusable
import kotlinx.coroutines.CoroutineScope
import javax.inject.Qualifier

@Module
object ActivityModule {
    @Provides
    @LifecycleScope
    fun providesCoroutineScope(appCompatActivity: AppCompatActivity): CoroutineScope = appCompatActivity.lifecycleScope
    
    @Provides
    @ActivityContext
    fun providesActiivtyContext(appCompatActivity: AppCompatActivity): Context = appCompatActivity

    @Reusable
    @Provides
    @ActivityContext
    fun providesGlideLoader(@ActivityContext context: Context): RequestManager = Glide.with(context)

    @Qualifier
    annotation class LifecycleScope
    
    @Qualifier
    annotation class ActivityContext
}