package software.eastshore.gifwall.ui.closeup

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.util.TypedValue
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.RequestManager
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import software.eastshore.gifwall.R
import software.eastshore.gifwall.applicationComponent
import software.eastshore.gifwall.di.ActivityModule
import software.eastshore.gifwall.giphy.HasGlide
import software.eastshore.gifwall.giphy.loadGiphyAttribution
import javax.inject.Inject

class CloseupActivity : AppCompatActivity(), HasGlide {
    private val viewModel: CloseupViewModel by viewModels()

    @ActivityModule.ActivityContext
    @Inject
    override lateinit var glide: RequestManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject()

        setContentView(R.layout.activity_closeup)

        viewModel.media =
            intent.getParcelableExtra(ARG_MEDIA) ?: savedInstanceState?.getParcelable(ARG_MEDIA)

        glide
            .load(viewModel.media?.images?.original?.gifUrl)
            .fitCenter()
            .into(findViewById(R.id.closeup_image))

        findViewById<Toolbar>(R.id.my_toolbar).let { toolbar ->
            setSupportActionBar(toolbar)

            supportActionBar?.apply {
                setDisplayHomeAsUpEnabled(true)
                setDisplayShowHomeEnabled(true)
            }
        }

        loadGiphyAttribution(this::findViewById, R.id.giphy_attribution)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.closeup_toolbar, menu)

        viewModel.media?.let { media ->
            Log.v(localClassName, "Name of media to show: ${media.title}")
            findViewById<Toolbar>(R.id.my_toolbar).title = media.title
        }

        val favorite = menu.findItem(R.id.favorite)

        lifecycleScope.launch {
            viewModel.isFavorite.collect { isFavorite ->
                Log.v(localClassName, "Checked status: $isFavorite")

                favorite.isChecked = isFavorite
                favorite.icon = ContextCompat.getDrawable(
                    this@CloseupActivity,
                    if (isFavorite) {
                        R.drawable.ic_baseline_star_24
                    } else {
                        R.drawable.ic_baseline_star_border_24
                    }
                )
            }
        }

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            android.R.id.home -> {
                returnToCloseup()
                true
            }
            R.id.share -> {
                share()
                true
            }
            R.id.favorite -> {
                toggleFavorite(item.isChecked)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    override fun onBackPressed() = returnToCloseup()

    private fun returnToCloseup() {
        setResult(RESULT_DONE, Intent().apply { putExtra(ARG_MEDIA, viewModel.media) })
        finish()
    }

    private fun share() {
        verifyStoragePermissions()
        lifecycleScope.launch {
            viewModel.shareableImage.collect { shareable ->
                Log.v(localClassName, "Got shareable file at $shareable")

                Intent(Intent.ACTION_SEND)
                    .apply {
                        putExtra(Intent.EXTRA_STREAM, shareable)
                        viewModel.media?.type?.name
                        type = "image/gif"
                    }
                    .let {
                        startActivity(Intent.createChooser(it, "Sharing GIF"))
                    }
            }
        }
    }

    private fun toggleFavorite(isChecked: Boolean) {
        lifecycleScope.launch {
            viewModel.setFavorite(isChecked).collect()
        }
    }

    private fun verifyStoragePermissions() {
        // Do not need in Q and up
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            return
        }

        when (ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )) {
            PackageManager.PERMISSION_GRANTED -> viewModel.isStorageReady = true
            else -> {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    REQUEST_FILE_PRIVS
                )
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_FILE_PRIVS -> {
                val externalWritePrivIndex =
                    permissions.indexOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                viewModel.isStorageReady = when (grantResults[externalWritePrivIndex]) {
                    PackageManager.PERMISSION_GRANTED -> true
                    else -> false
                }
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }

    }

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        outState.putParcelable(ARG_MEDIA, viewModel.media)
        super.onSaveInstanceState(outState, outPersistentState)
    }

    private fun inject() {
        applicationComponent.inject(viewModel)
        applicationComponent.activityComponentBuilder.binds(this).build().inject(this)
    }

    private fun themeAttribute(id: Int): Int =
        TypedValue().let { tv ->
            theme.resolveAttribute(id, tv, true)
            tv.data
        }

    companion object {
        const val ARG_MEDIA = "giphy.media"
        const val RESULT_DONE = 667
        private const val REQUEST_FILE_PRIVS = 665
    }
}