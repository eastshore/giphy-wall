package software.eastshore.gifwall.ui.main

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import android.view.*
import android.view.inputmethod.InputMethodManager
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.view.MenuProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.RequestManager
import com.bumptech.glide.integration.recyclerview.RecyclerViewPreloader
import com.giphy.sdk.core.models.Media
import com.giphy.sdk.tracking.position
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.sample
import kotlinx.coroutines.launch
import software.eastshore.gifwall.R
import software.eastshore.gifwall.applicationComponent
import software.eastshore.gifwall.di.AppModule
import software.eastshore.gifwall.giphy.HasGlide
import software.eastshore.gifwall.giphy.loadGiphyAttribution
import software.eastshore.gifwall.ui.closeup.CloseupActivity
import javax.inject.Inject
import kotlin.time.Duration.Companion.milliseconds

@OptIn(FlowPreview::class)
class FavoritesFragment : Fragment(), MenuProvider, HasGlide {
    private val viewModel: FavoritesViewModel by activityViewModels()

    @Inject
    lateinit var galleryAdapter: dagger.Lazy<GridAdapter>

    @AppModule.AppContext
    @Inject
    override lateinit var glide: RequestManager

    private lateinit var activityLauncher: ActivityResultLauncher<Intent>

    override fun onAttach(context: Context) {
        inject(context)

        activityLauncher = registerForActivityResult(
            ActivityResultContracts.StartActivityForResult(),
            ::onActivityResult
        )

        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater
            .inflate(R.layout.fragment_grid, container)
            .also(this@FavoritesFragment::bindViews)
    }

    override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
        menuInflater.inflate(R.menu.favorites_toolbar, menu)
    }

    override fun onMenuItemSelected(menuItem: MenuItem): Boolean = false

    @OptIn(ExperimentalCoroutinesApi::class)
    @SuppressLint("ClickableViewAccessibility")
    private fun bindViews(view: View) {
        val recycler: RecyclerView = view.findViewById(R.id.gallery)
        recycler.layoutManager = GridLayoutManager(view.context, spanCount)

        val adapter = galleryAdapter.get()
        recycler.adapter = adapter

        recycler.setOnTouchListener { _, _ ->
            (view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(
                recycler.windowToken,
                0
            )
            false
        }

        RecyclerViewPreloader(
            this@FavoritesFragment,
            adapter,
            GridPreloadCellSizeCalculation(recycler),
            50
        ).let(recycler::addOnScrollListener)

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.favoritesList
                .flatMapLatest {
                    Log.v(TAG, "Got new pager - updating!")
                    it.flow
                }
                .collectLatest {
                    recycler.scrollToPosition(0)
                    adapter.submitData(it)
                }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            adapter.clickFlow.collectLatest(viewModel::selectImage)
        }

        viewLifecycleOwner.lifecycleScope.launch {
            Log.v(TAG, "Register click listener")
            viewModel.onImageSelected
                .sample(75L.milliseconds)
                .onEach { Log.v(TAG, "Well, here comes a click") }
                .collectLatest { clickedImage ->
                    Log.v(
                        TAG,
                        "Clicked on ${clickedImage.position}, ${clickedImage.id}"
                    )

                    activityLauncher.launch(
                        Intent(context, CloseupActivity::class.java)
                            .apply { putExtra(CloseupActivity.ARG_MEDIA, clickedImage) })
                }
        }

        val loadingView: View = view.findViewById(R.id.gallery_loading)

        viewLifecycleOwner.lifecycleScope.launch {
            adapter.loadStateFlow.collectLatest { state ->
                when (state.refresh) {
                    is LoadState.Loading -> loadingView.visibility = View.VISIBLE
                    is LoadState.NotLoading -> loadingView.visibility = View.GONE
                    else -> View.GONE
                }
            }
        }

        val swipeContainer: SwipeRefreshLayout = view.findViewById(R.id.swipeContainer)

        swipeContainer.isRefreshing = false

        swipeContainer.setOnRefreshListener {
            loadingView.visibility = View.VISIBLE
            viewModel.refresh()
            if (swipeContainer.isRefreshing) {
                swipeContainer.isRefreshing = false
            }
        }

        TypedValue()
            .let { tv ->
                view.context.theme.resolveAttribute(R.attr.colorPrimaryVariant, tv, true)
                swipeContainer.setProgressBackgroundColorSchemeColor(tv.data)
            }

        TypedValue()
            .let { tv ->
                view.context.theme.resolveAttribute(R.attr.colorOnPrimary, tv, true)
                swipeContainer.setColorSchemeColors(tv.data)
            }

        swipeContainer.setSize(SwipeRefreshLayout.LARGE)

        loadGiphyAttribution(view::findViewById, R.id.giphy_attribution)
    }

    private fun onActivityResult(result: ActivityResult) {
        Log.v(TAG, "Result of activity: ${result.resultCode}")

        result.data
            ?.getParcelableExtra<Media>(CloseupActivity.ARG_MEDIA)
            ?.position
            ?.also { pos ->
                view?.findViewById<RecyclerView>(R.id.gallery)?.scrollToPosition(pos)
            }
            ?: Log.v(TAG, "No Scroll")
    }

    private val spanCount: Int
        get() = if (resources.displayMetrics.let { it.widthPixels > it.heightPixels }) {
            3
        } else {
            2
        }

    private fun inject(context: Context) {
        if (!this::galleryAdapter.isInitialized) {
            context.applicationComponent.inject(this)
            context.applicationComponent.inject(viewModel)
        }
    }

    companion object {
        private val TAG = FavoritesFragment::class.simpleName
    }

}