package software.eastshore.gifwall.ui.main

import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.DataSource
import androidx.paging.Pager
import androidx.paging.PagingConfig
import com.giphy.sdk.core.models.Media
import com.giphy.sdk.core.network.api.GPHApiClient
import com.giphy.sdk.tracking.position
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.*
import software.eastshore.gifwall.PREF_FAVORITES_IDS
import software.eastshore.gifwall.data.SharedPrefsDataSource
import software.eastshore.gifwall.di.AppModule
import software.eastshore.gifwall.giphy.FavoritesPositionalSource
import javax.inject.Inject

class FavoritesViewModel : ViewModel() {
    @Inject
    lateinit var giphyApi: GPHApiClient

    @Inject
    @AppModule.IODispatcher
    lateinit var ioDispatcher: CoroutineDispatcher

    @AppModule.AppContext
    @Inject
    lateinit var sharedPreferences: SharedPreferences

    @OptIn(ExperimentalCoroutinesApi::class)
    val favoritesList: Flow<Pager<Int, Media>>
        get() =
            onRefreshMutable
                .onStart { emit(Unit) }
                .flatMapLatest {
                    favorites
                        .map { it.toList().sorted() }
                        .flatMapLatest {
                            callbackFlow {
                                val source =  FavoritesPositionalSource(giphyApi, it, viewModelScope)
                                send(
                                    Pager(PagingConfig(pageSize = 25, initialLoadSize = 25)) {
                                        object : DataSource.Factory<Int, Media>() {
                                            override fun create(): DataSource<Int, Media> = source
                                        }.asPagingSourceFactory()()
                                    }
                                )
                                awaitClose {
                                    source.dump()
                                }
                            }
                        }
                }

    fun selectImage(image: Media) {
        if (!onImageSelectedMutable.tryEmit(image)) {
            Log.w(javaClass.simpleName, "Error emitting image for [${image.position}]")
        }
    }

    fun refresh() {
        onRefreshMutable.tryEmit(Unit)
    }

    private val prefsListener: SharedPrefsDataSource by lazy {
        SharedPrefsDataSource(sharedPreferences, viewModelScope, ioDispatcher)
    }

    private val onImageSelectedMutable: MutableSharedFlow<Media> by lazy {
        MutableSharedFlow(
            0,
            1,
            BufferOverflow.DROP_OLDEST
        )
    }

    val onImageSelected: Flow<Media> by lazy {
        onImageSelectedMutable.shareIn(viewModelScope, SharingStarted.Lazily)
    }

    private val onRefreshMutable: MutableSharedFlow<Unit> by lazy {
        MutableSharedFlow(
            0,
            1,
            BufferOverflow.DROP_OLDEST
        )
    }

    private val favorites: Flow<Set<String>> by lazy {
        prefsListener
            .watchField<Set<String>?>(PREF_FAVORITES_IDS) { sp, f ->
                sp.getStringSet(f, null)
            }
            .map { it ?: emptySet() }
            .stateIn(viewModelScope, SharingStarted.Eagerly, emptySet())
    }

}