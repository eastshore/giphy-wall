package software.eastshore.gifwall.ui.main

import android.annotation.SuppressLint
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.SearchView
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.MenuProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.RequestManager
import com.bumptech.glide.integration.recyclerview.RecyclerViewPreloader
import com.bumptech.glide.request.target.CustomViewTarget
import com.bumptech.glide.request.transition.Transition
import com.giphy.sdk.core.models.Media
import com.giphy.sdk.core.models.enums.RatingType
import com.giphy.sdk.tracking.position
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.sample
import kotlinx.coroutines.launch
import software.eastshore.gifwall.R
import software.eastshore.gifwall.applicationComponent
import software.eastshore.gifwall.di.AppModule
import software.eastshore.gifwall.giphy.HasGlide
import software.eastshore.gifwall.giphy.loadGiphyAttribution
import software.eastshore.gifwall.ui.closeup.CloseupActivity
import javax.inject.Inject
import kotlin.time.Duration.Companion.milliseconds

@OptIn(FlowPreview::class)
class GalleryFragment : Fragment(), MenuProvider, HasGlide {
    private val viewModel: MainViewModel by activityViewModels()

    @Inject
    lateinit var galleryAdapter: dagger.Lazy<GridAdapter>

    @AppModule.AppContext
    @Inject
    override lateinit var glide: RequestManager

    private lateinit var activityLauncher: ActivityResultLauncher<Intent>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState?.getString(SearchManager.QUERY)?.also(viewModel::updateQuery)
    }

    override fun onAttach(context: Context) {
        inject(context)

        activityLauncher = registerForActivityResult(
            ActivityResultContracts.StartActivityForResult(),
            ::onActivityResult
        )

        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater
            .inflate(R.layout.fragment_grid, container)
            .also(this::bindViews)
    }

    override fun onCreateMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.main_toolbar, menu)

        val searchActionView: MenuItem = menu.findItem(R.id.search)

        (searchActionView.actionView as SearchView).run {
            this.queryHint = getString(R.string.search_hint)

            setQuery(viewModel.searchQuery, false)

            setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    setQuery(query)
                    clearFocus()
                    searchActionView.collapseActionView()
                    this@GalleryFragment
                        .view
                        ?.findViewById<RecyclerView>(R.id.gallery)
                        ?.smoothScrollToPosition(0)
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    setQuery(newText)
                    return true
                }
            })

            isIconifiedByDefault = false
        }

        val g = menu.findItem(R.id.spice_g)
        val pg = menu.findItem(R.id.spice_PG)
        val pg13 = menu.findItem(R.id.spice_PG13)
        val r = menu.findItem(R.id.spice_R)
        val unrated = menu.findItem(R.id.spice_unrated)
        val nsfw = menu.findItem(R.id.spice_NSFW)

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.spiceLevelFlow
                .collectLatest {
                    when (it) {
                        RatingType.g -> g.isChecked = true
                        RatingType.pg -> pg.isChecked = true
                        RatingType.pg13 -> pg13.isChecked = true
                        RatingType.r -> r.isChecked = true
                        RatingType.unrated -> unrated.isChecked = true
                        RatingType.nsfw -> nsfw.isChecked = true
                        else -> {}
                    }
                }
        }
    }

    override fun onMenuItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            R.id.spice_NSFW -> {
                viewModel.chooseSpice(RatingType.nsfw)
                true
            }
            R.id.spice_unrated -> {
                viewModel.chooseSpice(RatingType.unrated)
                true
            }
            R.id.spice_R -> {
                viewModel.chooseSpice(RatingType.r)
                true
            }
            R.id.spice_PG13 -> {
                viewModel.chooseSpice(RatingType.pg13)
                true
            }
            R.id.spice_PG -> {
                viewModel.chooseSpice(RatingType.pg)
                true
            }
            R.id.spice_g -> {
                viewModel.chooseSpice(RatingType.g)
                true
            }
            else -> false
        }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString(SearchManager.QUERY, viewModel.searchQuery)
    }

    private fun setQuery(query: String?) {
        Log.v(javaClass.simpleName, "update query: $query")
        viewModel.updateQuery(query)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @SuppressLint("ClickableViewAccessibility")
    private fun bindViews(view: View) {
        val recycler: RecyclerView = view.findViewById(R.id.gallery)
        recycler.layoutManager = GridLayoutManager(view.context, spanCount)

        val adapter = galleryAdapter.get()
        recycler.adapter = adapter

        recycler.setOnTouchListener { _, _ ->
            (view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(
                recycler.windowToken,
                0
            )
            false
        }

        RecyclerViewPreloader(
            this@GalleryFragment,
            adapter,
            GridPreloadCellSizeCalculation(recycler),
            50
        ).let(recycler::addOnScrollListener)

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.results
                .flatMapLatest {
                    Log.v(javaClass.simpleName, "Got new pager - updating!")
                    it.flow
                }
                .collectLatest {
                    recycler.scrollToPosition(0)
                    adapter.submitData(it)
                }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            adapter.clickFlow.collectLatest(viewModel::selectImage)
        }

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.onImageSelected
                .sample(75L.milliseconds)
                .collectLatest { clickedImage ->
                    Log.v(
                        javaClass.simpleName,
                        "Clicked on ${clickedImage.position}, ${clickedImage.id}"
                    )

                    activityLauncher.launch(
                        Intent(context, CloseupActivity::class.java)
                            .apply { putExtra(CloseupActivity.ARG_MEDIA, clickedImage) })
                }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel
                .searchQueryFlow
                .sample(1000L)
                .collectLatest {
                    (requireActivity() as? AppCompatActivity)?.supportActionBar?.title =
                        if (it.isNullOrBlank()) {
                            getString(R.string.app_name)
                        } else {
                            it
                        }
                }
        }

        val loadingView: View = view.findViewById(R.id.gallery_loading)

        viewLifecycleOwner.lifecycleScope.launch {
            adapter.loadStateFlow.collectLatest { state ->
                when (state.refresh) {
                    is LoadState.Loading -> loadingView.visibility = View.VISIBLE
                    is LoadState.NotLoading -> loadingView.visibility = View.GONE
                    else -> View.GONE
                }
            }
        }

        val swipeContainer: SwipeRefreshLayout = view.findViewById(R.id.swipeContainer)

        swipeContainer.isRefreshing = false

        swipeContainer.setOnRefreshListener {
            loadingView.visibility = View.VISIBLE
            viewModel.refresh()
            if (swipeContainer.isRefreshing) {
                swipeContainer.isRefreshing = false
            }
        }

        TypedValue()
            .let { tv ->
                view.context.theme.resolveAttribute(R.attr.colorPrimaryVariant, tv, true)
                swipeContainer.setProgressBackgroundColorSchemeColor(tv.data)
            }

        TypedValue()
            .let { tv ->
                view.context.theme.resolveAttribute(R.attr.colorOnPrimary, tv, true)
                swipeContainer.setColorSchemeColors(tv.data)
            }

        swipeContainer.setSize(SwipeRefreshLayout.LARGE)

        loadGiphyAttribution(view::findViewById, R.id.giphy_attribution)
    }

    private fun onActivityResult(result: ActivityResult) {
        Log.v(javaClass.simpleName, "Result of activity: ${result.resultCode}")

        result.data
            ?.getParcelableExtra<Media>(CloseupActivity.ARG_MEDIA)
            ?.position
            ?.also { pos ->
                view?.findViewById<RecyclerView>(R.id.gallery)?.scrollToPosition(pos)
            }
            ?: Log.v(javaClass.simpleName, "No Scroll")
    }

    private val spanCount: Int
        get() = if (resources.displayMetrics.let { it.widthPixels > it.heightPixels }) {
            3
        } else {
            2
        }

    private fun inject(context: Context) {
        if (!this::galleryAdapter.isInitialized) {
            context.applicationComponent.inject(this)
            context.applicationComponent.inject(viewModel)
        }
    }

    companion object {
        class ViewBackgroundTarget<T : Drawable, V : View>(private val v: V) : CustomViewTarget<V, T>(v) {
            override fun onLoadFailed(errorDrawable: Drawable?) {
                errorDrawable?.let(v::setBackground)
            }

            override fun onResourceReady(resource: T, transition: Transition<in T>?) {
                v.background = resource
            }

            override fun onResourceCleared(placeholder: Drawable?) {
                placeholder?.let(v::setBackground)
            }

        }
    }
}