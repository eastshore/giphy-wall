package software.eastshore.gifwall.ui.main

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.ListPreloader

class GridPreloadCellSizeCalculation(
    private val recyclerView: RecyclerView
) : ListPreloader.PreloadSizeProvider<String> {
    override fun getPreloadSize(
        item: String,
        adapterPosition: Int,
        perItemPosition: Int
    ): IntArray =
        (recyclerView.width / (recyclerView.layoutManager as GridLayoutManager).spanCount)
            .let { size ->
                intArrayOf(size, size)
            }
}