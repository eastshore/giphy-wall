package software.eastshore.gifwall.ui.main

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.ListPreloader
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.RequestManager
import com.giphy.sdk.core.models.Media
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import software.eastshore.gifwall.R
import software.eastshore.gifwall.di.AppModule
import javax.inject.Inject

class GridAdapter
@Inject
constructor(
    diffCallback: DiffUtil.ItemCallback<Media>,
    @AppModule.AppContext private val glide: RequestManager
) :
    PagingDataAdapter<Media, GridAdapter.VH>(diffCallback),
    ListPreloader.PreloadModelProvider<String> {

    private val clickChannel: MutableSharedFlow<Media> by lazy {
        MutableSharedFlow(0, 1, BufferOverflow.DROP_OLDEST)
    }

    val clickFlow: Flow<Media> by lazy {
        clickChannel
    }

    class VH(view: View) : RecyclerView.ViewHolder(view) {
        val imageView: ImageView = view.findViewById(R.id.gallery_image)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.imageView.setImageResource(R.drawable.ic_baby_launcher_foreground)

        getItem(position)?.also { media ->
            media.galleryUrl?.let { url ->
                glide
                    .load(url)
                    .placeholder(R.drawable.ic_baby_launcher_foreground)
                    .optionalCenterCrop()
                    .into(holder.imageView)

                holder.itemView.setOnClickListener {
                    Log.v(javaClass.simpleName, "Clicking on item $position")
                    clickChannel.tryEmit(media)
                }
            }
        } ?: run {
            Log.w(javaClass.simpleName, "No item found for index $position")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return LayoutInflater.from(parent.context)!!.inflate(R.layout.grid_cell, parent, false)
            .let(GridAdapter::VH)
            .also {
                it.imageView.setImageResource(R.drawable.ic_baby_launcher_foreground)
            }
    }

    override fun getPreloadItems(position: Int): MutableList<String> =
        getItem(position)?.galleryUrl?.let { mutableListOf(it) } ?: mutableListOf()

    override fun getPreloadRequestBuilder(item: String): RequestBuilder<*> =
        glide
            .load(item)
            .placeholder(R.drawable.ic_baby_launcher_foreground)
            .optionalCenterCrop()

    private val Media.galleryUrl: String?
        get() =
            images.downsizedSmall?.gifUrl
                ?: images.downsized?.gifUrl
                ?: images.downsizedMedium?.gifUrl
                ?: images.downsizedLarge?.gifUrl
                ?: images.original?.gifUrl


}