package software.eastshore.gifwall.ui.closeup

import android.content.ContentResolver
import android.content.ContentValues
import android.content.SharedPreferences
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.Request
import com.bumptech.glide.request.target.SizeReadyCallback
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.request.transition.Transition
import com.giphy.sdk.core.models.Media
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.ChannelResult
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.*
import software.eastshore.gifwall.PREF_FAVORITES_IDS
import software.eastshore.gifwall.data.ImageGlider
import software.eastshore.gifwall.data.SharedPrefsDataSource
import software.eastshore.gifwall.di.AppModule
import java.io.File
import java.io.FileOutputStream
import javax.inject.Inject

class CloseupViewModel(
    savedStateHandle: SavedStateHandle
) : ViewModel() {
    @Inject
    @AppModule.AppContext
    lateinit var glide: RequestManager

    @Inject
    @AppModule.AppContext
    lateinit var contentResolver: ContentResolver

    @Inject
    @AppModule.IODispatcher
    lateinit var ioDispatcher: CoroutineDispatcher

    @Inject
    @AppModule.MainDispatcher
    lateinit var mainDispatcher: CoroutineDispatcher

    @Inject
    @AppModule.AppContext
    lateinit var prefs: SharedPreferences

    private val mutableMedia: MutableStateFlow<Media?> = MutableStateFlow(savedStateHandle["media"])
    private val storageIsReady: MutableStateFlow<Boolean> = MutableStateFlow(false)

    var media: Media?
        set(value) {
            mutableMedia.tryEmit(value)
        }
        get() = mutableMedia.value

    var isStorageReady: Boolean
        set(value) {
            storageIsReady.tryEmit(value)
        }
        get() = storageIsReady.value

    @OptIn(ExperimentalCoroutinesApi::class)
    private val copiedMediaFlow: Flow<Uri> by lazy {
        mutableMedia
            .filterNotNull()
            .flatMapLatest { media ->
                glideImageFlowFor(media).map { media to it }
            }
            .map { (media, file) ->
                val details = ContentValues().apply {
                    put(MediaStore.Images.Media.DISPLAY_NAME, media.filename)
                }
                val uri: Uri?

                withContext(ioDispatcher) {
                    uri = contentResolver.insert(mediaCollection, details)

                    uri
                        ?.let {
                            contentResolver.openFileDescriptor(it, "w", null)
                        }
                        ?.use {
                            file.inputStream().use { from ->
                                FileOutputStream(it.fileDescriptor).use { to ->
                                    from.copyTo(to)
                                }
                            }
                        } ?: throw java.lang.RuntimeException("Could not write to MediaStore")
                    uri
                }
            }
            .filterNotNull()
    }

    val shareableImage: Flow<Uri> by lazy {
        copiedMediaFlow
            .map<Uri, Uri?> { it }
            .stateIn(viewModelScope, SharingStarted.Lazily, null)
            .filterNotNull()
    }

    fun setFavorite(fav: Boolean): Flow<Unit> =
        combine(
            favorites,
            mutableMedia.map { it?.id }.filterNotNull()
        ) { fs, m ->
            if (fav) {
                fs - m
            } else {
                fs + m
            }
        }.map { newFaves ->
            prefListener.updateAsync {
                it.putStringSet(
                    PREF_FAVORITES_IDS,
                    newFaves
                )
            }
        }.take(1)

    private fun glideImageFlowFor(media: Media): Flow<File> =
        callbackFlow {
            val gliding: Target<File> = glide
                .download(media.images.original?.gifUrl)
                .into(ImageGlider(this@callbackFlow))

            awaitClose {
                glide.clear(gliding)
            }
        }

    private val Media.filename: String
        get() {
            val name = title ?: "giphy.gif"

            return if (name.endsWith(".gif")) {
                name
            } else {
                "$name.gif"
            }
        }

    private val mediaCollection
        get() =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                MediaStore.Images.Media.getContentUri(MediaStore.VOLUME_EXTERNAL_PRIMARY)
            } else {
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            }

    private val prefListener by lazy {
        SharedPrefsDataSource(
            prefs,
            viewModelScope,
            ioDispatcher
        )
    }

    private val favorites: Flow<Set<String>> by lazy {
        prefListener
            .watchField<Set<String>?>(PREF_FAVORITES_IDS) { sp, f ->
                sp.getStringSet(f, null)
            }
            .map { it ?: emptySet() }
            .stateIn(viewModelScope, SharingStarted.Eagerly, emptySet())
    }

    val isFavorite: Flow<Boolean> by lazy {
        mutableMedia
            .combine(favorites) { m, f ->
                m?.id in f
            }
            .stateIn(viewModelScope, SharingStarted.Eagerly, false)
    }

    companion object {
    }
}