package software.eastshore.gifwall.ui.main

import android.app.SearchManager
import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.MenuProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.lifecycleScope
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import software.eastshore.gifwall.R
import software.eastshore.gifwall.applicationComponent
import java.lang.ref.WeakReference

class MainActivity : AppCompatActivity() {

    private val viewModel: MainViewModel by viewModels()
    private lateinit var pager: ViewPager2
    private lateinit var adapter: Adapter
    private var menuProvider: MenuProvider? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        applicationComponent.inject(viewModel)

        setContentView(R.layout.activity_main)

        pager = findViewById(R.id.view_pager)
        adapter = Adapter(this)
        pager.adapter = adapter

        setSupportActionBar(findViewById(R.id.my_toolbar))

        lifecycleScope.launch {
            viewModel
                .hasFavorites
                .collectLatest { hasFavorites ->
                    if (hasFavorites != adapter.hasFavorites) {
                        adapter.hasFavorites = hasFavorites
                        adapter.notifyItemChanged(1)
                    }
                }
        }

        pager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                menuProvider?.let(this@MainActivity::removeMenuProvider)
                val fragment: Fragment? = supportFragmentManager.fragments.firstOrNull {
                    when (position) {
                        0 -> it is GalleryFragment
                        1 -> it is FavoritesFragment
                        else -> false
                    }
                }
                (fragment as? MenuProvider)?.let { provider ->
                    addMenuProvider(provider)
                    menuProvider = provider
                }
                supportActionBar?.title =
                    when (fragment) {
                        is GalleryFragment ->
                            viewModel.searchQuery.let { query ->
                                if (query.isNullOrBlank()) {
                                    getString(R.string.app_name)
                                } else {
                                    query
                                }
                            }
                        is FavoritesFragment -> getString(R.string.title_favorites)
                        else -> getString(R.string.app_name)
                    }
            }
        })
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        Log.v(localClassName, "New Intent Received!")
        intent?.also(this::consumeIntent)
    }

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        super.onSaveInstanceState(outState, outPersistentState)
        outState.putString(SearchManager.QUERY, viewModel.searchQuery)
    }

    private fun setQuery(query: String?) {
        Log.v(localClassName, "update query: $query")
        viewModel.updateQuery(query)
    }

    private fun consumeIntent(intent: Intent) {
        when (intent.action) {
            Intent.ACTION_SEARCH -> {
                intent.getStringExtra(SearchManager.QUERY).also { query ->
                    if (query != viewModel.searchQuery) {
                        setQuery(query)

                        if (this::pager.isInitialized) {
                            pager.currentItem = 0
                        }
                    }
                }
            }
        }
    }

    companion object {
        private class Adapter(activity: FragmentActivity) : FragmentStateAdapter(activity) {
            var hasFavorites: Boolean = false

            override fun getItemCount(): Int = if (hasFavorites) {
                2
            } else {
                1
            }

            override fun createFragment(position: Int): Fragment = when (position) {
                0 -> GalleryFragment().also { fragmentHandles.put(position, WeakReference(it)) }
                1 -> FavoritesFragment().also { fragmentHandles.put(position, WeakReference(it)) }
                else -> throw IndexOutOfBoundsException("Index $position out of pager bounds!")
            }

            fun fetchFragment(position: Int): Fragment? = fragmentHandles[position]?.get()

            private val fragmentHandles: MutableMap<Int, WeakReference<Fragment>> = mutableMapOf()
        }
    }
}