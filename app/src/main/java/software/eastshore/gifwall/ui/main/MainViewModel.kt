package software.eastshore.gifwall.ui.main

import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.DataSource
import androidx.paging.Pager
import androidx.paging.PagingConfig
import com.giphy.sdk.core.models.Media
import com.giphy.sdk.core.models.enums.RatingType
import com.giphy.sdk.core.network.api.GPHApiClient
import com.giphy.sdk.tracking.position
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.*
import software.eastshore.gifwall.PREF_FAVORITES_IDS
import software.eastshore.gifwall.data.SharedPrefsDataSource
import software.eastshore.gifwall.di.AppModule
import software.eastshore.gifwall.giphy.FavoritesSource
import software.eastshore.gifwall.giphy.PositionalSearchSource
import javax.inject.Inject

class MainViewModel(
    private val savedStateHandle: SavedStateHandle
) : ViewModel() {
    @Inject
    lateinit var giphyApi: GPHApiClient

    @Inject
    @AppModule.IODispatcher
    lateinit var ioDispatcher: CoroutineDispatcher

    @AppModule.AppContext
    @Inject
    lateinit var sharedPreferences: SharedPreferences

    val searchQuery: String? get() = searchQueryMutable.value

    private val searchQueryMutable: MutableStateFlow<String?> =
        MutableStateFlow(savedStateHandle["query"])
    val searchQueryFlow: Flow<String?> = searchQueryMutable

    val spiceLevelFlow: Flow<RatingType?> by lazy {
        prefsListener
            .watchField(PREF_SPICE) { sp, key ->
                sp.takeIf { it.contains(key) }
                    ?.getString(key, null)
                    ?.let(RatingType::valueOf)
            }
            .stateIn(
                viewModelScope,
                SharingStarted.Eagerly,
                null
            )
    }

    fun chooseSpice(newSpice: RatingType): Unit =
        prefsListener.update { it.putString(PREF_SPICE, newSpice.name) }

    private val onImageSelectedMutable: MutableSharedFlow<Media> by lazy {
        MutableSharedFlow(
            0,
            1,
            BufferOverflow.DROP_OLDEST
        )
    }

    val onImageSelected: Flow<Media> by lazy {
        onImageSelectedMutable.shareIn(viewModelScope, SharingStarted.Lazily)
    }

    private val onRefreshMutable: MutableSharedFlow<Unit> by lazy {
        MutableSharedFlow(
            0,
            1,
            BufferOverflow.DROP_OLDEST
        )
    }

    fun refresh() {
        onRefreshMutable.tryEmit(Unit)
    }

    @OptIn(FlowPreview::class, ExperimentalCoroutinesApi::class)
    val results: Flow<Pager<Int, Media>>
        get() =
            onRefreshMutable
                .onStart { emit(Unit) }
                .combine(
                    searchQueryFlow
                        .map { it?.trim() }
                        .distinctUntilChanged()) { _, query -> query }
                .combine(spiceLevelFlow) { query, spice ->
                    Log.v(javaClass.simpleName, "New Query: $query")
                    query to spice
                }
                .sample(1500L)
                .flatMapLatest { (query, spice) ->
                    callbackFlow {
                        val source = PositionalSearchSource(giphyApi, query, spice ?: DEFAULT_SPICE)
                        send(Pager(PagingConfig(pageSize = 25, initialLoadSize = 25)) {
                            object : DataSource.Factory<Int, Media>() {
                                override fun create(): DataSource<Int, Media> = source
                            }.asPagingSourceFactory()()
                        })
                        awaitClose {
                            source.cancelAll()
                        }
                    }

                }
                .shareIn(viewModelScope, SharingStarted.Lazily)

    fun updateQuery(query: String?) {
        if (query != searchQueryMutable.value) {
            savedStateHandle["query"] = query
            if (!searchQueryMutable.tryEmit(query)) {
                Log.w(javaClass.simpleName, "Error emitting query [$query]")
            }
        }
    }

    fun selectImage(image: Media) {
        if (!onImageSelectedMutable.tryEmit(image)) {
            Log.w(javaClass.simpleName, "Error emitting image for [${image.position}]")
        }
    }

    private val favorites: Flow<Set<String>> by lazy {
        prefsListener
            .watchField<Set<String>?>(PREF_FAVORITES_IDS) { sp, f ->
                sp.getStringSet(f, null)
            }
            .map { it ?: emptySet() }
            .stateIn(viewModelScope, SharingStarted.Eagerly, emptySet())
    }

    val favoritesList: Flow<Pager<Int, Media>>
        get() = favorites
            .map { FavoritesSource(giphyApi, it.toList().sorted()) }
            .map { Pager(PagingConfig(0)) { it } }


    val hasFavorites: Flow<Boolean> by lazy {
        favorites
            .map(Set<String>::isNotEmpty)
            .stateIn(viewModelScope, SharingStarted.Eagerly, false)
    }

    private val prefsListener: SharedPrefsDataSource by lazy {
        SharedPrefsDataSource(sharedPreferences, viewModelScope, ioDispatcher)
    }

    companion object {
        val DEFAULT_SPICE: RatingType = RatingType.pg13
        private val PREF_SPICE = MainViewModel::class.simpleName + "(SpiceLevel)"
    }
}