# GIFWall

Show a grid of animated GIF results from Giphy, defaulting to the Giphy "trending" feed.

To build, you will need to add your own Giphy API key to local.properties:

```
giphy.apikey=MY_API_KEY
```

Additionally, you need the signing key's passwords to use the existing secret store. Else, you can 
replace the store and specify your own passwords and key:
```
secretstore.keyname=SOMEKEY
secretstore.password=STORE_PASSWORD
secretstore.keypassword=KEY_PASSWORD
```

## [Releases](https://gitlab.com/eastshore/giphy-wall/-/releases)

Add Giphy attribution to get an upgrade to the API token, make favorites loader more consistent.

[aug3_2022_0](https://gitlab.com/eastshore/giphy-wall/-/releases/aug3_2022_0)

Make "Favorites" load pages of favorites instead of all at once.

[aug2_2022_2](https://gitlab.com/eastshore/giphy-wall/-/releases/aug2_2022_0)

Add a "Favorites" page. Reach it by scrolling to the right from the main gallery view.

[aug2_2022_0](https://gitlab.com/eastshore/giphy-wall/-/releases/aug2_2022_0)

Rearrange the deckchairs on Glide requester instances, make spice level persistence more permanent.

[jul30_2022_0](https://gitlab.com/eastshore/giphy-wall/-/releases/jul30_2022_0)

Add pull-to-refresh, increase reliability of initial load

[jul27_2022_0](https://gitlab.com/eastshore/giphy-wall/-/releases/jul27_2022_0)

Second published build - fixed blank list on device rotation

[jul26_2022_1](https://gitlab.com/eastshore/giphy-wall/-/releases/jul26_2022_1)

First published build - ironed out some of the kinks, but background loading can still get "stuck", also activity can get stuck unpopulated in first load after tilt, etc.

[jul26_2022](https://gitlab.com/eastshore/giphy-wall/-/releases/jul26_2022)
